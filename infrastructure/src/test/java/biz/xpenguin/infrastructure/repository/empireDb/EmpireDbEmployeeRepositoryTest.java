/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.xpenguin.infrastructure.repository.empireDb;

import biz.xpenguin.infrastructure.repository.empireDb.dataModel.SampleDB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.empire.db.DBDatabaseDriver;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author nakajimayuji
 */
public class EmpireDbEmployeeRepositoryTest {

    private static final SampleDB db = new SampleDB();
    private static String jdbcClass = "org.hsqldb.jdbcDriver";
    private static String jdbcURL = "jdbc:hsqldb:mem:empiredb";
    private static String jdbcUser = "sa";
    private static String jdbcPwd = "";
    private static String empireDBDriverClass = "org.apache.empire.db.hsql.DBDatabaseDriverHSql";

    public EmpireDbEmployeeRepositoryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        try {
            Class.forName(EmpireDbEmployeeRepositoryTest.jdbcClass);
            Connection conn = DriverManager.getConnection(EmpireDbEmployeeRepositoryTest.jdbcURL, EmpireDbEmployeeRepositoryTest.jdbcUser, EmpireDbEmployeeRepositoryTest.jdbcPwd);
            DBDatabaseDriver driver = (DBDatabaseDriver) Class.forName(EmpireDbEmployeeRepositoryTest.empireDBDriverClass).newInstance();
            EmpireDbEmployeeRepositoryTest.db.open(driver, conn);
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class EmpireDbEmployeeRepository.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        EmpireDbEmployeeRepository instance = new EmpireDbEmployeeRepository();
        instance.setDb(EmpireDbEmployeeRepositoryTest.db);
        ArrayList expResult = null;
        ArrayList result = instance.findAll();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findById method, of class EmpireDbEmployeeRepository.
     */
    @Test
    public void testFindById() {
        System.out.println("findById");
        Integer id = 1;
        EmpireDbEmployeeRepository instance = new EmpireDbEmployeeRepository();
        instance.setDb(EmpireDbEmployeeRepositoryTest.db);
        HashMap expResult = null;
        HashMap result = instance.findById(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findByDept method, of class EmpireDbEmployeeRepository.
     */
    @Test
    public void testFindByDept() {
        System.out.println("findByDept");
        String deptName = "";
        EmpireDbEmployeeRepository instance = new EmpireDbEmployeeRepository();
        instance.setDb(EmpireDbEmployeeRepositoryTest.db);
        ArrayList expResult = null;
        ArrayList result = instance.findByDept(deptName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}