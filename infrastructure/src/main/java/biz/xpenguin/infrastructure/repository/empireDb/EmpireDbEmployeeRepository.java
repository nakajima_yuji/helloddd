/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.xpenguin.infrastructure.repository.empireDb;

import biz.xpenguin.infrastructure.repository.EmployeeRepository;
import biz.xpenguin.infrastructure.repository.empireDb.dataModel.SampleDB;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.empire.db.DBCmdParam;
import org.apache.empire.db.DBCmpType;
import org.apache.empire.db.DBCommand;
import org.apache.empire.db.expr.compare.DBExistsExpr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nakajimayuji
 */
public class EmpireDbEmployeeRepository implements EmployeeRepository {

    private static final Logger log = LoggerFactory.getLogger(EmpireDbEmployeeRepository.class);

    private SampleDB db;

    public void setDb(SampleDB db) {
        this.db = db;
    }

    @Override
    public ArrayList<HashMap<String, String>> findAll() {
        DBCommand cmd = db.createCommand();
        cmd.select(db.EMPLOYEES.getColumns());
        log.info(cmd.getSelect());
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HashMap<String, String> findById(Integer id) {
        DBCommand cmd = db.createCommand();
        DBCmdParam paramId = cmd.addParam(db.EMPLOYEES.EMPLOYEE_ID, id);
        cmd.select(db.EMPLOYEES.getColumns());
        cmd.where(db.EMPLOYEES.EMPLOYEE_ID.is(paramId));
        log.info(cmd.getSelect());
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<HashMap<String, String>> findByDept(String deptName) {
        DBCommand cmd = db.createCommand();
        DBCommand cmdSub = db.createCommand();
        DBCmdParam paramName = cmd.addParam(db.DEPARTMENTS.NAME, deptName);
        cmdSub.where(db.DEPARTMENTS.DEPARTMENT_ID.is(db.EMPLOYEES.getAlias() + "." + db.EMPLOYEES.DEPARTMENT_ID.getName()));
        cmdSub.where(db.DEPARTMENTS.NAME.is(paramName));
        cmdSub.select(db.getValueExpr("X"));
        cmd.select(db.EMPLOYEES.getColumns());
        cmd.where(new DBExistsExpr(cmdSub));
        log.info(cmd.getSelect());
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
