/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.xpenguin.infrastructure.repository.empireDb.dataModel;

import org.apache.empire.db.DBDatabase;

/**
 *
 * from Empire-db sample
 */
public class SampleDB extends DBDatabase {

    public final Departments DEPARTMENTS = new Departments(this);
    public final Employees EMPLOYEES = new Employees(this);

    public SampleDB() {
    }
}
