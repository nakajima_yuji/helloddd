/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.xpenguin.infrastructure.repository;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author nakajimayuji
 */
public interface EmployeeRepository {

    public ArrayList<HashMap<String, String>> findAll();
    public HashMap<String, String> findById(Integer id);
    public ArrayList<HashMap<String, String>> findByDept(String deptName);
}
